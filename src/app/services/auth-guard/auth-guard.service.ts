import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let auth = JSON.parse(localStorage.getItem('cfair'));
    if (auth && auth.token) {
      if (route.routeConfig.path === 'login') {
        this.router.navigate(['/main']);
      }

      let blockIds = [
        'windomhill@yuhan.co.kr',
        'juha.hong@zeiss.com',
        'jesung84@gmail.com',
        'julie.an@leica-microsystems.com',
        'kic3770@jeilpharm.co.kr',
        'Lia.Chae@advancedbionics.com',
        'brandon.lim@dasanlab.net',
        'dhj0307@ahn-gook.com',
        'eungguk.lee@hanmi.co.kr',
        'juhee.song@fphcare.co.kr',
        'henry.son@hearlife.co.kr',
        'dhhan@samapharm.co.kr',
        'kwg@hearingmedics.co.kr',
        'ki-hyeong.k.kim@gsk.com',
        'chasu@hanlim.com',
        'hyejin.cho@bbraun.com',
        'suyoung@donga.co.kr',
        'lovestory119@gccorp.com',
        'khkwon1@ilyang.co.kr',
      ];

      let result = blockIds.indexOf(auth.email);
      if (result > -1 && route.routeConfig.path !== 'e-booth') {
        alert('접근 불가한 계정입니다.');
        return false;
      }

      return true;
    } else {
      // 아래 path는 로그인 상태에서만 접근 가능
      let path = [
        'live', 'vod', 'vod/:vodId',
        'tpps', 'tpps/:tppId', 'posters', 'posters/:posterId',
        'about', 'organization', 'program', 'e-booth', 'speakers',
        'my-page'
      ];

      // console.log(route.routeConfig.path)
      let result = path.indexOf(route.routeConfig.path);
      if (result > -1) {
        alert('Login is required.');
        this.router.navigate(['/login']);
      }
      return true;
    }
  }
}

