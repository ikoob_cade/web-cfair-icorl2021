import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {
    private isReceiveMessage: BehaviorSubject<any> = new BehaviorSubject(null);
    currentReceiveMessage = this.isReceiveMessage.asObservable();

    constructor() {
    }

    changeReceiveMessage(data: any) {
        this.isReceiveMessage.next(data);
    }
}
