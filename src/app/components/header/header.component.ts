import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MemberService } from '../../services/api/member.service';
import { AuthService } from '../../services/auth/auth.service';
import { SocketService } from '../../services/socket/socket.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public user: any;
  public menus: any = [
    { title: 'OVERVIEW', router: '/about' },
    { title: 'ORGANIZATION', router: '/organization' },
    { title: 'PROGRAM', router: '/program' },
    { title: 'LIVE', router: '/live', queryParams: { dateId: '5fd02819c25b63001133f6eb', roomId: '605aa6f338f3180012288713', } },
    { title: 'SPEAKERS', router: '/speakers' },
    { title: 'TPP', router: '/tpps' },
    { title: 'POSTERS', router: '/posters' },
    { title: 'SPONSORS', router: '/e-booth' },
    { title: 'NOTICE', router: '/board' },
  ];

  collapse = true;

  constructor(
    private router: Router,
    private authService: AuthService,
    private memberService: MemberService,
    private socketService: SocketService,
  ) {
    router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((val) => {
        const auth = JSON.parse(localStorage.getItem('cfair'));
        this.user = (auth && auth.token) ? auth : undefined;
        // console.log(this.user)

        if (this.user) {
          this.loginSocket();
        }

        $('.navbar-collapse').collapse('hide');
        this.collapse = true;
      });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  postAttendance = (): void => {
    this.memberService.attendance(this.user.id).subscribe(res => { });
  }

  /** 로그아웃 */
  logout(): void {
    this.authService.logout().subscribe(res => {
      this.logoutSocket();

      localStorage.removeItem('cfair');
      this.router.navigate(['/']);
    }, error => {
      localStorage.removeItem('cfair');
      this.router.navigate(['/']);
    });
  }

  loginSocket() {
    this.socketService.login({
      memberId: this.user.id,
      token: this.user.token
    });
  }

  logoutSocket() {
    this.socketService.logout({
      memberId: this.user.id
    });
  }


}
